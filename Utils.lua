local JSON = require('JSON')

-- Utils for LGS
local Utils = {}

function Utils.contains(t, element)
	for _, v in pairs(t) do
		if v == element then
			return true
		end
	end
	return false
end

function Utils.find(t, element)
	for i, v in ipairs(t) do
		if v == element then
			return i
		end
	end
	return -1
end

function Utils.remove(t, element)
	for i, v in ipairs(t) do
		if v == element then
			table.remove(t, i)
			return true
		end
	end
	return false
end

function Utils.rectIntersect(x1, y1, w1, h1, x2, y2, w2, h2)
	--print(string.format('x1 %d y1 %d w1 %d h1 %d x2 %d y2 %d w2 %d h2 %d', x1, y1, w1, h1, x2, y2, w2, h2))
	local intersectX = math.max(x1, x2)
        local intersectY = math.max(y1, y2)
        local intersectW = math.min(x1 + w1, x2 + w2) - intersectX
        local intersectH = math.min(y1 + h1, y2 + h2) - intersectY
	return intersectX, intersectY, intersectW, intersectH
end

function Utils.clamp(val, min, max)
	if val > max then
		return max
	end
	if val < min then
		return min
	end
	return val
end

function Utils.round(x)
	return x>=0 and math.floor(x+0.5) or math.ceil(x-0.5)
end

function Utils.split(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={} ; i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    t[i] = str
    i = i + 1
  end
  return t
end

function Utils.shuffle(t)
	local n = #t
    while n > 1 do
        local k = math.random(n)
        t[n], t[k] = t[k], t[n]
        n = n - 1
    end
    return t
end

function Utils.clone(obj)
	local copy = {}
	for k, v in pairs(obj) do
		copy[k] = v
	end
	setmetatable(copy, getmetatable(obj))
	return copy
end

Utils.json = JSON

return Utils
