-- Colour class for LGS

local Colour = {}

function Colour:new(opts)
	if opts == nil then opts = {} end
	local col = {
		r = opts.r or 0,
		g = opts.g or 0,
		b = opts.b or 0,
		a = opts.a or 1
	}
	setmetatable(col, self)
	self.__index = self
	return col
end

function Colour:white()
	return self:new({r = 1.0, g = 1.0, b = 1.0})
end

function Colour:black()
	return self:new()
end

function Colour:red()
	return self:new({r = 1.0, g = 0.0, b = 0.0})
end

function Colour:green()
	return self:new({r = 0.0, g = 1.0, b = 0.0})
end

function Colour:blue()
	return self:new({r = 0.0, g = 0.0, b = 1.0})
end

function Colour:clear()
	return self:new({r = 1.0, g = 1.0, b = 1.0, a = 0.0})
end

return Colour
