-- Events class for LGS
local Utils = require 'Utils'

local Events = {}

function Events:new()
	local events = {register = {}}
	setmetatable(events, self)
	self.__index = self
	return events
end

function Events:add(event, listener)
	-- initialise listener list for event if necessary
	if self.register[event] == nil then self.register[event] = {} end
	-- add listener to list for event if not already registered
	if not Utils.contains(self.register[event], listener) then
		table.insert(self.register[event], listener)
	end
end

function Events:remove(event, listener)
	if self.register[event] ~= nil then
		local pos = Utils.find(self.register[event], listener)
		if pos >= 0 then
			table.remove(self.register[event], pos)
		end
	end
end

function Events:clear(event)
	-- delete the listener list for the event
	self.register[event] = nil
end

function Events:emit(event, data)
	if self.register[event] then
		for i, listener in ipairs(self.register[event]) do
			listener(event, data)
		end
	end
end

return Events
