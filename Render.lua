-- Requires torch
require 'torch'

-- Render class for LGS
local Vector2 = require 'Vector2'
local Colour = require 'Colour'
local Utils = require 'Utils'
local Bitmap = require 'Bitmap'

local Render = {}

function Render:new(opts)
	if opts == nil then opts = {} end
	local render = {
		width = opts.width or 64,
		height = opts.height or 32,
		camera = opts.camera or Vector2:one(),
		cameraEnabled = true,
		channels = opts.channels or 3
	}
	render.canvas = Bitmap:new({ channels = render.channels, height = render.height, width = render.width })
	setmetatable(render, self)
	self.__index = self
	return render
end

function Render:draw(bitmap, x, y)
	x = x or 1
	y = y or 1
	if self.cameraEnabled then
		x = x - (self.camera.x - 1)
		y = y - (self.camera.y - 1)
	end
	-- find the rectangle of intersection
	local iX, iY, iW, iH = Utils.rectIntersect(1, 1, self.width, self.height, x, y, bitmap.width, bitmap.height)
	-- exit early if we have no intersection to colour
	if iW < 1 or iH < 1 then
		return
	end
	local canvasRangeW = { iX, iX + iW - 1}
	local canvasRangeH = { iY, iY + iH - 1}
	--print(string.format('CanvasRangeW: %d %d, CanvasRangeH: %d %d', iX, iX + iW - 1, iY, iY + iH - 1))
	local bitmapRangeW = { canvasRangeW[1] - (x-1), canvasRangeW[2] - (x-1) }
	local bitmapRangeH = { canvasRangeH[1] - (y-1), canvasRangeH[2] - (y-1) }
	--print(string.format('BitmapRangeW: %d %d, BitmapRangeH: %d %d', bitmapRangeW[1], bitmapRangeW[2], bitmapRangeH[1], bitmapRangeH[2]))
	-- draw to canvas
	for i = 1, self.canvas.pixels:size(1) do -- channels
		self.canvas.pixels[{i, canvasRangeH, canvasRangeW}]:map2(bitmap.pixels[{i, bitmapRangeH, bitmapRangeW}], bitmap.alpha[{bitmapRangeH, bitmapRangeW}],
			function(colourCanvas, colourBitmap, alphaBitmap)
				return Utils.clamp(colourCanvas * (1 - alphaBitmap) + colourBitmap * alphaBitmap, 0.0, 1.0)
			end)
	end
end

function Render:text(font, text, x, y)
	x = x or 1
	y = y or 1
	-- iterate characters, draw glyphs, and update position
	local glyph
	for i = 1, text:len() do
		local char = text:sub(i, i)
		if char == ' ' then
			x = x + font.spaceWidth + font.spacing
		else
			glyph = font:glyph(char)
			self:draw(glyph, x, y)
			x = x + glyph.width + font.spacing
		end
	end
end

return Render
