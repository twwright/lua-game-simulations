-- GameObject base class
local Vector2 = require 'Vector2'

local GameObject = {}

function GameObject:new(opts)
	if opts == nil then opts = {} end
	local go = {
		id = opts.id,
		tags = opts.tags or {},
		collider = opts.collider,
		position = opts.position or Vector2:zero()
	}
	setmetatable(go, self)
	self.__index = self
	return go
end

-- called when game starts
function GameObject:init(game)
	-- do nothing
end

-- called each frame before drawing
function GameObject:update(game)
	-- do nothing
end

-- called each frame after updating
function GameObject:draw(game)
	-- do nothing
end

-- called when the object collides with another
function GameObject:collide(other)
	-- do nothing
end

return GameObject
