
local GameListener = require 'GameListener'
local Utils = require 'Utils'
local WebSocketSync = require('websocket').client.sync
local base64 = require 'base64'

local WebSocketGameListener = GameListener:new()

function WebSocketGameListener:new(opts)
  opts = opts or {}
  local listener = {
    host = opts.host,
    inputEnabled = opts.inputEnabled or true,
    timeout = opts.timeout or 0.1,
    ws = WebSocketSync({ timeout = opts.timeout or 0.1}),
    name = opts.name or 'LuaWebSocketGameListener',
    registered = false,
    verbose = opts.verbose or false
  }
  listener.ws.on_close = function() if self.verbose then print('WS closed') end end
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function WebSocketGameListener:init(game)
  -- connect the websocket
  local ok, err = self.ws:connect(self.host)
  if not ok then
    if self.verbose then
      print(err)
    end
    return
  end
  -- register the game
  local inputsStr = ''
  for input, val in pairs(game.input.inputs) do
    inputsStr = inputsStr .. input .. ';'
  end
  inputsStr = inputsStr:sub(1, -2)

  self:send(string.format('register %s %s', self.name, game.name))
  local resp = self.ws:receive()
  respSplit = Utils.split(resp)
  if respSplit[1] == 'ok' then
    self.registered = true
    -- send initial update of game details
    local update = {
      width = game.render.width,
      height = game.render.height,
      canvas = self:encodeState(game),
      score = game.score,
      inputs = inputsStr
    }
    self:send('update ' .. Utils.json:encode(update))
  else
    if self.verbose then
      print('Registration failed: ' .. resp)
    end
  end
end

function WebSocketGameListener:preUpdate(game)
  if self.ws.state == 'OPEN' then
    -- process pending messages
    local args
    repeat
      local receive = self.ws:receive()
      if receive then
          if self.verbose then
            print('Pending: ' .. receive:sub(1, 20))
          end
          args = Utils.split(receive)
          if args[1] == 'update' then
            local update = Utils.json:decode(args[2])
            if update.input then
              local activeInputs = Utils.split(update.input)
              for i, v in ipairs(activeInputs) do
                game.input.inputs[v] = true
              end
            end
          end
      end
    until not receive

    -- request input
    --[[
    if self.inputEnabled then
      local resp = self:send('requestInput')
      local split = Utils.split(resp, ' ')
      if split[1] ~= 'ok' then
        print('Error requesting inputs! (' .. resp .. ')')
      else
        local activeInputs = Utils.split(split[2] or '', ';')
        print('Inputs: ' .. resp)
      end
    end
    --]]
  end
end

function WebSocketGameListener:postUpdate(game)
  if self.ws.state == 'OPEN' then
    -- send update to server
    local update = {
      canvas = self:encodeState(game),
      score = game.score
    }
    self:send('update ' .. Utils.json:encode(update))
  end
end

function WebSocketGameListener:finish(game)
  if self.ws.state == 'OPEN' then
    -- close the WebSocket
    self.ws:close()
  end
end

function WebSocketGameListener:send(msg)
  local resp = nil
  if self.ws.state == 'OPEN' then
    -- send msg
    local ok = self.ws:send(msg)
    if not ok then
      if self.verbose then
        print('WS Error sending')
      end
      return
    end
  end
end

function WebSocketGameListener:encodeState(game)
  -- return a single element array with the game screen
  return { self.encodeScreen(game.render.canvas.pixels) }
end

function WebSocketGameListener.encodeScreen(screen)
  -- create string of canvas
  local canvasStr = ''
  for h = 1, screen:size(2) do
    for w = 1, screen:size(3) do
      for c = 1, screen:size(1) do
        canvasStr = canvasStr .. string.char(math.floor(screen[c][h][w] * 255))
      end
    end
  end
  -- base 64 encode
  return base64.encode(canvasStr)
end

return WebSocketGameListener
