local Vector2 = require 'Vector2'
local Utils = require 'Utils'

local Collider = {}

function Collider:new(opts)
	opts = opts or {}
	local collider = {
		position = opts.position or Vector2:one(),
		width = opts.width or 1,
		height = opts.height or 1,
		solid = opts.solid or false,
		static = opts.static or false
	}
	setmetatable(collider, self)
	self.__index = self
	return collider
end

function Collider:isColliding(other)
	 return self.position.x + self.width > other.position.x
		and self.position.y + self.height > other.position.y
		and other.position.x + other.width > self.position.x
		and other.position.y + other.height > self.position.y
end

function Collider:resolve(other)
	-- calculate distance to separate along each cardinal direction
	local separateLeft = other.position.x - (self.position.x + self.width)
	local separateRight = (other.position.x + other.width) - self.position.x
	local separateUp = (other.position.y + other.height) - self.position.y
	local separateDown = other.position.y - (self.position.y + self.height)

	-- determine the smallest separation along each axis
	local separate = Vector2:new(
		(math.abs(separateLeft) < math.abs(separateRight)) and separateLeft or separateRight,
		(math.abs(separateUp) < math.abs(separateDown)) and separateUp or separateDown)

	-- zero the larger axis, leaving a vector to move self out of intersection
	if math.abs(separate.x) < math.abs(separate.y) then
		separate.y = 0
	else
		separate.x = 0
	end

	-- apply the separation to the two colliders, depending on static properties
	if other.static then
		self.position:add(separate.x, separate.y)
	elseif self.static then
		other.position:add(-separate.x, -separate.y)
	else
		local half = separate:scl(0.5)
		self.position:add(Utils.round(half.x + 0.001), Utils.round(half.y + 0.001)) -- add tiny bit to force consistent rounding
		other.position:add(Utils.round(-half.x + 0.001), Utils.round(-half.y + 0.001)) -- subtract tiny bit for rounding
	end
end

return Collider
