
local GameListener = {}

function GameListener:new(opts)
  opts = opts or {}
  local listener = {}
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function GameListener:init()
  -- override this to do stuff when the game is first started
end

function GameListener:preUpdate(game)
  -- override this to do stuff before game:step() is called
end

function GameListener:postUpdate(game)
  -- override this to do stuff after game:step() is called
end

function GameListener:finish(game)
  -- override this to do stuff after terminal game:step() is called
end

return GameListener
