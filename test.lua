-- LGS Test File
require 'LGS_Utils'
require 'Vector2'
require 'Game'
require 'GameObject'
require 'TestObject'

-- TEST: create base game object and call empty update and draw
print('TEST: create TestObject and call update and draw')
local baseGo = GameObject:new()

baseGo:update()

baseGo:draw()

-- TEST: create TestObject and call update and draw
print('TEST: create TestObject and call update and draw')
local testGo = TestObject:new()

testGo:update()
testGo:draw()

-- TEST: add TestObject to game, then remove
print('TEST: add TestObject to game, then remove')
local game = Game:new()
print(#game.objects)
game:add(testGo)
print(#game.objects)
game:remove(testGo)
print(#game.objects)

-- TEST: add 2x TestObject to game using :addAll(), call step() and observe update and draw
print('TEST: add 2x TestObject to game using :addAll(), call step() and observe update and draw')
local otherGo = GameObject:new()
function otherGo:draw(game)
	print('In :draw() of otherGo')
end
function otherGo:update(game)
	print('In :update() of otherGo')
end

game:addAll({testGo, otherGo})
game:step()

-- TEST: add existing objects again, call :step()
print('TEST: add existing objects again, call :step()')
game:addAll({testGo, otherGo})
game:step()

-- TEST: add a new object that removes a different object during :update() (test pending add)
print('TEST: add a new object that removes a different object during :update() (test pending add)')
local deleteGo = GameObject:new({id = 'deleteId'})
function deleteGo:update(game)
	print('In :update() of deleteGo')
end
local deletingGo = GameObject:new()
function deletingGo:update(game)
	local go = game:find('deleteId')
	game:remove(go)
	print('In :update() of deletingGo. Removed deleteGo')
	print('#game.objects = ' .. #game.objects)
end

game:addAll({deletingGo, deleteGo})
game:step()
print('After :step(), #game.objects = ' .. #game.objects)