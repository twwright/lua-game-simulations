-- TestObject - testing object for GameObject

TestObject = GameObject:new()

function TestObject:draw(game)
	print('In :draw() of TestObject')
end

function TestObject:update(game)
	print('In :update() of TestObject')
end