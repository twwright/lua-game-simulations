-- Game - maintains the GameObject collection and runs iterations of the main game loop
local Utils = require 'Utils'
local Render = require 'Render'
local Events = require 'Events'
local Input = require 'Input'

local Game = {}

function Game:new(opts)
	opts = opts or {}
	local game = {
		name = opts.name or 'game',
		objects = {},
		pendingAdds = {},
		pendingRemoves = {},
		iterating = false,
		running = true,
		score = 0,
		render = Render:new(opts.render),
		input = Input:new({ inputs = opts.inputs }),
		events = Events:new(),
		background = opts.background or Colour:white(),
		listeners = opts.listeners or {}
	}
	setmetatable(game, self)
	self.__index = self
	return game
end

function Game:add(go)
	if self.iterating then
		-- add to pending adds
		if not Utils.contains(self.pendingAdds, go) then
			table.insert(self.pendingAdds, go)
		end
		-- remove from pending removes
		local pos = Utils.find(self.pendingRemoves, go)
		if pos >= 0 then
			table.remove(self.pendingRemoves, pos)
		end
	else
		if not Utils.contains(self.objects, go) then
			table.insert(self.objects, go)
		end
	end
end

function Game:addAll(arrayGo)
	for _, go in pairs(arrayGo) do
		self:add(go)
	end
end

function Game:remove(go)
	if self.iterating then
		-- add to pending removes
		if not Utils.contains(self.pendingRemoves, go) then
			table.insert(self.pendingRemoves, go)
		end
		-- remove from pending adds
		local pos = Utils.find(self.pendingAdds, go)
		if pos >= 0 then
			table.remove(self.pendingAdds, pos)
		end
	else
		local pos = Utils.find(self.objects, go)
		if pos >= 0 then
			table.remove(self.objects, pos)
		end
	end
end

function Game:removeAll(arrayGo)
	for _, go in pairs(arrayGo) do
		self:remove(go)
	end
end

function Game:processPending()
	for _, go in pairs(self.pendingAdds) do
		self:add(go)
	end
	for _, go in pairs(self.pendingRemoves) do
		self:remove(go)
	end
	-- clear pending lists
	for k, _ in pairs(self.pendingAdds) do
		self.pendingAdds[k] = nil
	end
	for k, _ in pairs(self.pendingRemoves) do
		self.pendingRemoves[k] = nil
	end
end

function Game:find(id)
	for _, go in pairs(self.objects) do
		if go.id == id then
			return go
		end
	end
	return nil
end

function Game:findAll(tag)
	local found = {}
	for _, go in pairs(self.objects) do
		if Utils.contains(go.tags, tag) then
			table.insert(found, go)
		end
	end
	return found
end

function Game:init()
	self.iterating = true
	-- initialise the game objects
	for _, go in pairs(self.objects) do
		go:init(self)
	end
	-- initialise the listeners
  for _, listener in pairs(self.listeners) do
    listener:init(self)
  end
	self.iterating = false
end

function Game:step(skip_listeners)
	-- notify listeners that update is about to occur
	if not skip_listeners then
		for _, listener in ipairs(self.listeners) do
			listener:preUpdate(self)
		end
	end

	-- update objects
	self:update()

	-- resolve collisions
	self:collide()

	-- handle all the pending objects
	self:processPending()

	-- draw objects
	self.render.canvas:fill(self.background)
	self:draw()

	-- update input cache
	self.input:update()

	-- notify listeners that update has finished
	if not skip_listeners then
		for _, listener in ipairs(self.listeners) do
			listener:postUpdate(self)
		end
	end
end

function Game:update()
	self.iterating = true
	for _, go in pairs(self.objects) do
		go:update(self)
	end
	self.iterating = false
end

function Game:collide()
	self.iterating = true
	for i, go in ipairs(self.objects) do
		if go.collider ~= nil then
			for j, otherGo in ipairs(self.objects) do
				if i > j
				and go ~= otherGo
				and otherGo.collider ~= nil
				and (not go.collider.static or not otherGo.collider.static) then
					if go.collider:isColliding(otherGo.collider) then
						-- callback to game objects to respond to collision
						go:collide(self, otherGo)
						otherGo:collide(self, go)
						-- resolve collision if both objects are solid
						if go.collider.solid and otherGo.collider.solid then
							go.collider:resolve(otherGo.collider)
							go.position:set(go.collider.position)
							otherGo.position:set(otherGo.collider.position)
						end
					end
				end
			end
		end
	end
	self.iterating = false
end

function Game:draw()
	self.iterating = true
	for _, go in pairs(self.objects) do
		go:draw(self)
	end
	self.iterating = false
end

return Game
