-- requires LGS stuff
local Bitmap = require 'Bitmap'
local Utils = require 'Utils'

local Font = {}

function Font:new(opts)
  assert(opts.glyphs, 'Glyphs table must exist!')
  opts = opts or {}
  local font = {
    spacing = opts.spacing or 1,
    unknown = opts.unknown,
    glyphs = opts.glyphs,
    spaceWidth = opts.spaceWidth or 3
  }
  setmetatable(font, self)
  self.__index = self
  return font
end

function Font:load(file)
  -- open the glyph descriptor file
  local fileIn = torch.DiskFile(file, 'r')
  fileIn:quiet()
  -- read the first line of the file to locate the atlas file
  local atlasFile = fileIn:readString("*l")
  local finalSlash = file:reverse():find('/')
  local prefix
  if finalSlash then
    prefix = file:sub(1, -finalSlash)
  else
    prefix = ''
  end
  local atlas = Bitmap:load(prefix .. atlasFile) -- append atlasFile to the directory portion of file
  -- read character spacing
  local spacing = tonumber(fileIn:readString("*l"))
  local spaceWidth = tonumber(fileIn:readString("*l"))
  -- read spacewidth
  -- read unknon glyph
  local glyphDescSplit = Utils.split(fileIn:readString("*l"), '\t')
  local glyphX = tonumber(glyphDescSplit[1])
  local glyphY = tonumber(glyphDescSplit[2])
  local glyphW = tonumber(glyphDescSplit[3])
  local glyphH = tonumber(glyphDescSplit[4])
  local unknownGlyph = atlas:crop(glyphX, glyphY, glyphW, glyphH)
  -- read glyphs from the descriptor
  local glyphs = {} -- table to store all the characters we open
  local glyph
  while not fileIn:hasError() do
    -- read a glyph descriptor from file
    local glyphDesc = fileIn:readString("*l")
    if glyphDesc:len() > 0 then
      glyphDescSplit = Utils.split(glyphDesc, '\t')
      glyphX = tonumber(glyphDescSplit[2])
      glyphY = tonumber(glyphDescSplit[3])
      glyphW = tonumber(glyphDescSplit[4])
      glyphH = tonumber(glyphDescSplit[5])
      glyph = atlas:crop(glyphX, glyphY, glyphW, glyphH)
      glyphs[glyphDescSplit[1]] = glyph
    end
  end
  return Font:new({unknown = unknownGlyph, glyphs = glyphs, spacing = spacing, spaceWidth = spaceWidth})
end

function Font:glyph(char)
  return self.glyphs[char] or self.unknown
end

function Font:size(text)
  -- iterate over string and total
  local width = 0
  local height = 0
  local glyph
  for i = 1, text:len() do
    glyph = self:glyph(text[i])
    width = width + (glyph.width + self.spacing)
    height = math.max(height, glyph.height)
  end
  -- subtract one spacing to correct for loop
  width = width - self.spacing
  return width, height
end

return Font
