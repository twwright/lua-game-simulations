local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'

local container = {}

container.loadAssets = function()
  -- store all the game assets as a table
  local assets = {}
  -- load the sprite bitmaps
  local gameAtlas = Bitmap:load('atlas2.png')
  assets.bitmaps = {
    wall    =   gameAtlas:crop(1, 1, 8, 8),
    player  =   gameAtlas:crop(9, 1, 8, 8),
    door    =   gameAtlas:crop(17, 1, 8, 8),
    door2   =   gameAtlas:crop(25, 1, 8, 8),
    sage    =   gameAtlas:crop(33, 1, 8, 8),
    blackKnight    =   gameAtlas:crop(41, 1, 8, 8),
    key     =   gameAtlas:crop(49, 1, 8, 8),
    death   =   gameAtlas:crop(57, 1, 8, 8),
    edge    =   gameAtlas:crop(1, 9, 8, 8),
    corner  =   gameAtlas:crop(9, 9, 8, 8),
    grass   =   gameAtlas:crop(17, 9, 8, 8),
    grass2  =   gameAtlas:crop(25, 9, 8, 8),
    chest   =   gameAtlas:crop(33, 9, 8, 8),
    whiteKnight    =   gameAtlas:crop(41, 9, 8, 8),
    armour  =   gameAtlas:crop(49, 9, 8, 8),
    blank   =   gameAtlas:crop(57, 9, 8, 8)
  }

  -- load the font
  assets.font = Font:load('pixel.font')

  container.assets = assets
end

container.createGame = function()
  -- create the game
  local game = Game:new({
    name = "honours",
    inputs = {"up", "down", "left", "right", "select"},
    render = {
      width = 96,
      height = 54
    }
  }) -- TODO: parameters for creating the game

  -- store the assets in the game
  game.assets = container.assets

  -- set up some game variables that we will be using
  game.goal = 1
  game.goals = { "goal1", "goal2", "goal3" }
  -- create the player object
  local player = GameObject:new({
    id = "player",
    position = Vector2:new(4, 4),
    collider = Collider:new({ position = Vector2:new(4, 4), width = 8, height = 8})
  })
  player.move = 4

  function player:update(game)
      if game.input:isOn("up") then
        self.position.y = self.position.y - self.move
      end
      if game.input:isOn("down") then
        self.position.y = self.position.y + self.move
      end
      if game.input:isOn("left") then
        self.position.x = self.position.x - self.move
      end
      if game.input:isOn("right") then
        self.position.x = self.position.x + self.move
      end
      self.collider.position:set(self.position)
  end

  function player:draw(game)
      game.render:draw(game.assets.bitmaps.player, self.position.x, self.position.y, 6, 3)
  end

  function player:collide(game, other)
    if other.id ~= nil then
      if other.id == game.goals[game.goal] then
        game.score = game.score + 100
        game.goal = game.goal + 1
      else
        game.score = game.score - 50
      end
      self.position:set(4, 4)

      self.collider.position:set(self.position)
    end
  end

  game:add(player)

  -- create the score/text objects
  local scoreWidth, scoreHeight = game.assets.font:size("SCORE ")
  local score = GameObject:new({position = Vector2:new(2, game.render.canvas.height - scoreHeight)})
  function score:draw(game)
    game.render:text(game.assets.font, "SCORE " .. game.score, self.position.x, self.position.y)
  end

  game:add(score)

  local goalW, goalH = game.assets.font:size("GOAL " .. game.goal)
  local goalText = GameObject:new({
    position = Vector2:new(game.render.canvas.width - goalW, game.render.canvas.height - goalH)
  })
  function goalText:draw(game)
    game.render:text(game.assets.font, "GOAL " .. game.goal, self.position.x, self.position.y)
  end

  game:add(goalText)

  -- add goals
  local goal = GameObject:new({
    position = Vector2:new(1, 1),
    collider = Collider:new({position = Vector2:new(1, 1), width = 8, height = 8})
  })

  function goal:update(game)
    -- nothing
  end

  function goal:draw(game)
    game.render:draw(self.bitmap, self.position.x, self.position.y)
  end

  function goal:collide(game, other)
    -- Nothing!
  end

  print(goal.metatable)

  local goal1 = Utils.clone(goal)
  goal1.bitmap = game.assets.bitmaps.sage
  goal1.position = Vector2:new(50, 8)
  goal1.collider = Collider:new({position = Vector2:new(50, 8), width = 8, height = 8})
  goal1.id = "goal1"

  local goal2 = Utils.clone(goal)
  goal2.bitmap = game.assets.bitmaps.chest
  goal2.position = Vector2:new(32, 24)
  goal2.collider = Collider:new({position = Vector2:new(32, 24), width = 8, height = 8})
  goal2.id = "goal2"

  game:addAll({goal1, goal2})
  return game
end

return container
