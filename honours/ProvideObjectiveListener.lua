local GameListener = require 'GameListener'
local Utils = require 'Utils'

local ProvideObjectiveListener = GameListener:new()

function ProvideObjectiveListener:new(opts)
  opts = opts or {}
  local listener = {
    objective_frames = {},
    objective_count = 0
  }
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function ProvideObjectiveListener:init(game)
  -- reset objective frames and pointer
  self.objective_frames = {}
  self.objective_count = 0
  self.zero_screen = game.render.canvas.pixels:clone():zero()
  self.saving_objectives = true
  -- register listeners to swap from saving to displaying
  game.events:add("enterGameScreen", function(event, data)
    self.saving_objectives = false
  end)
  game.events:add("enterGoalScreen", function(event, data)
    self.saving_objectives = true
    self.objective_frames = {} -- just started a new sequence of objectives
    self.objective_count = 0
  end)
  -- create game._state for use
  local screen_sizes = game.render.canvas.pixels:size()
  local state_sizes = torch.LongStorage(1 + #screen_sizes)
  for i = 1, #screen_sizes do
    state_sizes[i + 1] = screen_sizes[i]
  end
  state_sizes[1] = 2
  game._state = torch.zeros(state_sizes):float()
end

function ProvideObjectiveListener:preUpdate(game)
  -- check saving the currently displayed objective
  -- and display the first objective if we are moving to game mode
  if self.saving_objectives and game.input:justOn('select') then
    -- record the objective frame in the correct position
    self.objective_frames[game.goal] = game.render.canvas.pixels:clone()
  end
  -- cache a couple of variables to compare in postUpdate
  self._score = game.score
  self._goal = game.goal
end

function ProvideObjectiveListener:postUpdate(game)
  -- handle displaying the next objective frame if player just picked up a correct objective
  if not self.saving_objectives then
    self.objective_count = game.goal -- move to next goal
    if self.objective_count > game.numGoals then
      self.objective_count = 0 -- reset to nothing, we are at the game over screen
    end
  end
  -- update game._state
  game._state[1]:copy(game.render.canvas.pixels)
  local extra_frame = self.zero_screen
  if self.objective_count > 0 then extra_frame = self.objective_frames[self.objective_count] end
  game._state[2]:copy(extra_frame)
end

return ProvideObjectiveListener
