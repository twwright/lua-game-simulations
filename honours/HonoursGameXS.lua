local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'
local Colour = require 'Colour'
local Vector2 = require 'Vector2'
local Collider = require 'Collider'

local container = {}

container.loadAssets = function(opt)
  opt = opt or {}
  opt.atlas_file = opt.atlas_file or 'atlas2.png'
  opt.font_file = opt.font_file or 'pixel.font'

  -- store all the game assets as a table
  local assets = {}
  -- load the sprite bitmaps
  local gameAtlas = Bitmap:load(opt.atlas_file)
  assets.bitmaps = {
    wall    =   gameAtlas:crop(1, 1, 8, 8),
    player  =   gameAtlas:crop(9, 1, 8, 8),
    door    =   gameAtlas:crop(17, 1, 8, 8),
    door2   =   gameAtlas:crop(25, 1, 8, 8),
    sage    =   gameAtlas:crop(33, 1, 8, 8),
    blackKnight    =   gameAtlas:crop(41, 1, 8, 8),
    key     =   gameAtlas:crop(49, 1, 8, 8),
    death   =   gameAtlas:crop(57, 1, 8, 8),
    edge    =   gameAtlas:crop(1, 9, 8, 8),
    corner  =   gameAtlas:crop(9, 9, 8, 8),
    grass   =   gameAtlas:crop(17, 9, 8, 8),
    grass2  =   gameAtlas:crop(25, 9, 8, 8),
    chest   =   gameAtlas:crop(33, 9, 8, 8),
    whiteKnight    =   gameAtlas:crop(41, 9, 8, 8),
    armour  =   gameAtlas:crop(49, 9, 8, 8),
    heart   =   gameAtlas:crop(57, 9, 7, 6)
  }

  -- manipulate edge and corner to create all variations
  assets.bitmaps.edges = {
    ht = assets.bitmaps.edge:manip('V'),
    hb = assets.bitmaps.edge,
    vl = assets.bitmaps.edge:manip('R'),
    vr = assets.bitmaps.edge:manip('L')
  }

  assets.bitmaps.corners = {
    tr = assets.bitmaps.corner,
    tl = assets.bitmaps.corner:manip('L'),
    br = assets.bitmaps.corner:manip('R'),
    bl = assets.bitmaps.corner:manip('VH')
  }

  -- load the font
  assets.font = Font:load(opt.font_file)

  container.assets = assets
end

container.createGame = function(opt)
  opt = opt or {}
  -- create the game
  local game = Game:new({
    background = Colour:white(),
    name = "honours",
    inputs = {"none", "up", "down", "left", "right", "select", "quit"},
    render = {
      width = 48,
      height = 32,
      channels = 1,
    }
  })

  -- store the assets in the game
  game.assets = container.assets

  -- set up some game variables that we will be using
  game.goal = 1
  game.numGoals = 1
  game.maxNumGoals = 6
  game.numGoalObjects = 3
  game.numScreenObjects = 4
  game.numGoalTypes = 9

  game.stepScore = 0
  game.goalScore = 4
  game.badGoalScore = -0.1

  game.playerMove = 4
  game.playerMinBounds = Vector2:new(2, 2) -- top-left corner
  game.playerMaxBounds = Vector2:new(game.render.width - game.assets.bitmaps.player.width, game.render.height - 6 - game.assets.bitmaps.player.height)
  game.playerStart = Vector2:new((game.playerMaxBounds.x + game.playerMinBounds.x) / 2, (game.playerMaxBounds.y + game.playerMinBounds.y) / 2)
  game.goalObjects = {} -- cache to store goal objects for deletion later

  game.colliderPad = 1
  game.initSpawns = function(space)
    return {
      Vector2:new(2, 2), -- top left
      Vector2:new(2, 18), -- bottom left
      Vector2:new(40, 2), -- top right
      Vector2:new(40, 18), -- bottom right
    }
  end

  game.allGoals = {
    {
      name = "sage",
      bitmap = game.assets.bitmaps.sage
    },
    {
      name = "key",
      bitmap = game.assets.bitmaps.key
    },
    {
      name = "chest",
      bitmap = game.assets.bitmaps.chest
    },
    {
      name = "door",
      bitmap = game.assets.bitmaps.door
    },
    {
      name = "door2",
      bitmap = game.assets.bitmaps.door2
    },
    {
      name = "blackKnight",
      bitmap = game.assets.bitmaps.blackKnight
    },
    {
      name = "whiteKnight",
      bitmap = game.assets.bitmaps.whiteKnight
    },
    {
      name = "death",
      bitmap = game.assets.bitmaps.death
    },
    {
      name = "armour",
      bitmap = game.assets.bitmaps.armour
    }
  }

  -- add a game object to kick off game with an event
  local startGameObj = GameObject:new()
  function startGameObj:init(game)
    game.score = 0
    game.objectSpawns = game.initSpawns(game.objectSpacing)
    game.events:emit("enterGoalScreen")
  end
  game:add(startGameObj)

  -- create event listeners
  game.events:add("enterGoalScreen", function(event, data)
    -- check numGoalTypes is valid
    game.numGoalTypes = math.max(game.numGoalTypes, game.numGoals, game.numScreenObjects, game.numGoalObjects)
    -- init goals
    local goals = {}
    local goalStr = ""
    local rand
    for i = 1, game.numGoals do
      repeat
        rand = math.random(game.numGoalTypes)
        goalStr = goalStr .. rand
        goals[i] = rand
      until i == 1 or goals[i] ~= goals[i-1]
    end

    game.goal = 1
    game.goals = goals

    -- clear game
    game.objects = {}

    -- create the score/text objects
    local scoreWidth, scoreHeight = game.assets.font:size("SCORE")
    local score = GameObject:new({position = Vector2:new(2, game.render.canvas.height - scoreHeight)})
    function score:draw(game)
      local s = "" .. game.score
      game.render:text(game.assets.font, s, self.position.x, self.position.y)
    end
    game:add(score)

    -- create the goal text objects
    local goalW, goalH = game.assets.font:size("GOAL " .. game.goal)
    local goalText = GameObject:new({
      position = Vector2:new(game.render.canvas.width - goalW + 1, game.render.canvas.height - goalH)
    })
    function goalText:draw(game)
      game.render:text(game.assets.font, "GOAL " .. game.goal, self.position.x, self.position.y)
    end

    game:add(goalText)

    -- create an object to receive user input and advance screen
    local inputObj = GameObject:new()
    function inputObj:update(game)
      if game.input:justOn('select') then
        if game.goal < #game.goals then
          game.goal = game.goal + 1
          game.events:emit("nextGoalScreen")
        else
          game.goal = 1
          game.events:emit("enterGameScreen")
        end
      end
    end

    game:add(inputObj)

    -- with the goal screen set up, initialise some objects on-screen
    game.events:emit("nextGoalScreen")
  end)

  game.events:add("nextGoalScreen", function(event, data)
    -- remove any goal objects
    game:removeAll(game.goalObjects)

    -- generate a list of goal objects to display on the screen (including the true goal)
    local goalIndices = { game.goals[game.goal] }
    local rand
    for i = 2, game.numGoalObjects do -- start at 2 because we already added the index of current true goal
      repeat
        rand = math.random(game.numGoalTypes)
      until not Utils.contains(goalIndices, rand)
      table.insert(goalIndices, rand)
    end
    Utils.shuffle(goalIndices)

    -- generate the goal objects
    local goalDraw = GameObject:new()
    function goalDraw:draw(game)
      -- render goal
      local x = self.position.x
      local y = self.position.y
      game.render:draw(game.allGoals[self.goalIndex].bitmap, self.position.x, self.position.y)
      -- render border
      if self.goalIndex == game.goals[game.goal] then
        game.render:draw(game.assets.bitmaps.corners.tl, x - 8, y - 8) -- top-left
        game.render:draw(game.assets.bitmaps.corners.br, x + 8, y + 8) -- bottom-right
        game.render:draw(game.assets.bitmaps.corners.bl, x - 8, y + 8) -- bottom-left
        game.render:draw(game.assets.bitmaps.corners.tr, x + 8, y - 8) -- top-right
        game.render:draw(game.assets.bitmaps.edges.vr, x - 8, y) -- left
        game.render:draw(game.assets.bitmaps.edges.vl, x + 8, y) -- right
        game.render:draw(game.assets.bitmaps.edges.ht, x, y + 8) -- bottom
        game.render:draw(game.assets.bitmaps.edges.hb, x, y - 8) -- top
      end
    end

    local obj
    local goalSpace = 12
    game.goalObjects = {} -- clear the existing goal objects
    -- calculate the space required by the goals
    local width = (math.min(#goalIndices, 3) - 0.5) * goalSpace
    local height = (math.ceil(#goalIndices / 3) - 0.5) * goalSpace
    -- calculate where the goal drawing will start
    local _, goalH = game.assets.font:size("GOAL " .. game.goal)
    local x = game.render.width/2 - width/2
    local y = (game.render.height - goalH)/2 - height/2
    for i = 1, #goalIndices do
      obj = Utils.clone(goalDraw)
      obj.position = Vector2:new(x + ((i-1) % 3) * goalSpace, y + math.floor((i-1) / 3) * goalSpace)
      obj.goalIndex = goalIndices[i]
      table.insert(game.goalObjects, obj)
    end
    game:addAll(game.goalObjects)

  end)

  game.events:add("enterGameScreen", function(event, data)
    -- clear game
    game.objects = {}

    -- create the score/text objects
    local scoreWidth, scoreHeight = game.assets.font:size("SCORE")
    local score = GameObject:new({position = Vector2:new(2, game.render.canvas.height - scoreHeight)})
    function score:draw(game)
      local s = "" .. game.score
      game.render:text(game.assets.font, s, self.position.x, self.position.y)
    end
    game:add(score)

    local goalW, goalH = game.assets.font:size("GOAL " .. game.goal)
    local goalText = GameObject:new({
      position = Vector2:new(game.render.canvas.width - goalW + 1, game.render.canvas.height - goalH)
    })
    function goalText:draw(game)
      game.render:text(game.assets.font, "GOAL " .. game.goal, self.position.x, self.position.y)
    end
    game:add(goalText)

    -- generate goals and add them
    -- generate a list of goal objects to display on the screen (including the true goal)
    local goalIndices = { game.goals[game.goal] }
    local rand
    for i = 2, game.numScreenObjects do -- start at 2 because we already added the index of current true goal
      repeat
        rand = math.random(game.numGoalTypes)
      until not Utils.contains(goalIndices, rand)
      table.insert(goalIndices, rand)
    end
    Utils.shuffle(goalIndices)

    -- generate the goal objects
    local goalObj = GameObject:new()
    function goalObj:draw(game)
      -- render goal
      local x = self.position.x
      local y = self.position.y
      game.render:draw(game.allGoals[self.goalIndex].bitmap, self.position.x, self.position.y)
    end

    local obj
    game.goalObjects = {} -- clear the existing goal objects
    local spawnIndices = {}
    local spawnIndex
    for i = 1, #goalIndices do
      -- generate the spawn index
      repeat
        spawnIndex = math.random(#game.objectSpawns)
      until not Utils.contains(spawnIndices, spawnIndex)
      table.insert(spawnIndices, spawnIndex)
      -- create the object
      obj = Utils.clone(goalObj)
      obj.position = Vector2:new(game.objectSpawns[spawnIndex].x, game.objectSpawns[spawnIndex].y)
      obj.collider = Collider:new({ width = 8 - game.colliderPad*2, height = 8 - game.colliderPad*2, position = Vector2:new():set(obj.position):add(game.colliderPad, game.colliderPad)})
      obj.goalIndex = goalIndices[i]
      table.insert(game.goalObjects, obj)
    end
    game:addAll(game.goalObjects)

    -- add player
    -- create the player object
    local player = GameObject:new({
      id = "player",
      position = Vector2:new(game.playerStart.x , game.playerStart.y),
      collider = Collider:new({ position = Vector2:new(game.playerStart.x + game.colliderPad, game.playerStart.y + game.colliderPad), width = 8 - game.colliderPad*2, height = 8 - game.colliderPad*2})
    })
    player.move = game.playerMove

    function player:update(game)
        local x = self.position.x
        local y = self.position.y
        if game.input:isOn("up") then
          self.position.y = self.position.y - self.move
        end
        if game.input:isOn("down") then
          self.position.y = self.position.y + self.move
        end
        if game.input:isOn("left") then
          self.position.x = self.position.x - self.move
        end
        if game.input:isOn("right") then
          self.position.x = self.position.x + self.move
        end
        -- check bounds
        if self.position.x < game.playerMinBounds.x then self.position.x = game.playerMinBounds.x end
        if self.position.y < game.playerMinBounds.y then self.position.y = game.playerMinBounds.y end
        if self.position.x > game.playerMaxBounds.x then self.position.x = game.playerMaxBounds.x end
        if self.position.y > game.playerMaxBounds.y then self.position.y = game.playerMaxBounds.y end
        -- match collider position with player
        self.collider.position:set(self.position):add(game.colliderPad, game.colliderPad)
        -- if player moved, decrease score slightly
        if x ~= self.position.x or y ~= self.position.y then
          game.score = game.score + game.stepScore
          if game.score < 0 then game.score = 0 end
        end
    end

    function player:draw(game)
        game.render:draw(game.assets.bitmaps.player, self.position.x, self.position.y)
    end

    function player:collide(game, other)
      if other.goalIndex ~= nil then
        if other.goalIndex == game.goals[game.goal] then
          game.score = game.score + game.goalScore
          game.goal = game.goal + 1
          if game.goal > #game.goals then
            if #game.goals == game.maxNumGoals then
              game.events:emit("gameOver")
            else
              game.numGoals = game.numGoals + 1
              game.events:emit("enterGoalScreen")
            end
          else
            game.events:emit("enterGameScreen")
          end
        else
          game.score = game.score + game.badGoalScore
          game.events:emit("gameOver")
        end
      end
    end

    game:add(player)

  end)

  game.events:add("gameOver", function(event, data)
    game.objects = {}

    game.running = false
    -- add game over text
    local gameOverW, gameOverH = game.assets.font:size("GAME OVER")
    local disp_score = math.ceil(math.max(0, game.score))
    local scoreW, scoreH = game.assets.font:size("" .. disp_score)
    local gameOverText = GameObject:new({
      position = Vector2:new(game.render.canvas.width/2, game.render.canvas.height/2)
    })
    function gameOverText:draw(game)
      game.render:text(game.assets.font, "GAME OVER", self.position.x - gameOverW / 2, self.position.y - gameOverH / 2)
      game.render:text(game.assets.font, "" .. disp_score, self.position.x - scoreW / 2, self.position.y + gameOverH)
    end
    function gameOverText:update(game)
      game.running = false -- game over
    end
    game:add(gameOverText)
  end)

  return game
end

return container
