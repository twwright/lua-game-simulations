local Utils = require 'Utils'

local env = {}

-- constructor
function env:init(opt)
  opt = opt or {}
  self.opt = opt
  -- call reset to create the game
  self:reset(opt)
  self._reward = 0
  self._term = false
  self.actions = { "up", "down", "left", "right" }
  for _, bad_action in pairs(opt.remove_actions or {}) do
    Utils.remove(self.actions, bad_action)
  end
  return self
end

-- retrieve the current state of the game: screen, current reward, if the game is over
function env:getState()
  return self._state, self._reward, self._term
end

-- reset the game state to the beginning of the game
function env:reset(opt)
  -- initialise the game state
  -- 1: if high reward is up, 2: if high reward is down, 3: can move up, 4: can move down, 5: can move left, 6: can move right
  self._state = torch.ByteTensor(6):zero()
  self.is_high_reward_up = math.random() < 0.5
  self.x_pos = 1
  self.y_pos = 0
  self.length = opt.tmaze_length
  self.reward_high = opt.tmaze_reward_high
  self.reward_low = opt.tmaze_reward_low
  self:updateState()
  -- clear temporary variables
  self._reward = 0
  self._term = false
end

-- advance the game one step, returning the screen state, reward at this step, if the game is still running, and the number of lives
function env:step(action, training)
  if not self._term then
    -- perform horizontal movement
    if action == "left" then
      self.x_pos = self.x_pos - 1
    elseif action == "right" then
      self.x_pos = self.x_pos + 1
    end
    self.x_pos = Utils.clamp(self.x_pos, 1, self.length)

    -- vertical movement if at the final spot
    if self.x_pos == self.length then
      if action == "up" then
        self.y_pos = 1
      elseif action == "down" then
        self.y_pos = -1
      end
    end

    -- update the state
    self:updateState()
  end
  return self:getState()
end

-- starts a new game and returns the state of it
function env:newGame()
  self:reset(self.opt)
  return self:getState()
end

-- restarts the game and then takes k no-op actions and returns the state of it
function env:nextRandomGame(k)
  return self:newGame()
end

function env:nObsFeature()
  return self._state:nElement()
end

function env:getActions()
  return self.actions
end

function env:updateState()
  -- update the state
  self._state:zero()
  if self.y_pos == 0 then
    self._term = false
    self._state[5] = 1 -- can move left initially
    self._state[6] = 1 -- can move right initially
    if self.x_pos == 1 then -- at the start
      self._state[5] = 0 -- disable moving left
      if self.is_high_reward_up then
        self._state[1] = 1 -- indicate that at the T, high reward is up
      else
        self._state[2] = 1 -- indicate that at the T, high reward is down
      end
    elseif self.x_pos == self.length then
      self._state[6] = 0 -- disable moving right
      self._state[3] = 1 -- can move up
      self._state[4] = 1 -- can move down
    end
  elseif self.y_pos == 1 then
    self._state[4] = 1 -- can move down
    if self.is_high_reward_up then self._reward = self.reward_high else self._reward = self.reward_low end
    self._term = true
  elseif self.y_pos == -1 then
    self._state[3] = 1 -- can move up
    if not self.is_high_reward_up then self._reward = self.reward_high else self._reward = self.reward_low end
    self._term = true
  end
end

return env
