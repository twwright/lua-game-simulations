local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'
local Colour = require 'Colour'
local Vector2 = require 'Vector2'
local Collider = require 'Collider'

local container = {}

container.loadAssets = function(opt)
  opt = opt or {}

  -- store all the game assets as a table
  local assets = {}

  assets.bitmaps = {
    player = Bitmap:new({ width = 2, height = 2, channels = 1}),
    goal1 = Bitmap:new({ width = 3, height = 3, channels = 1}),
    goal2 = Bitmap:new({ width = 3, height = 3, channels = 1}),
    win = Bitmap:new({ width = 6, height = 4, channels = 1}),
    lose = Bitmap:new({ width = 6, height = 4, channels = 1})
  }

  local b = Colour:black()
  local w = Colour:white()

  assets.bitmaps.player:fill(b)

  assets.bitmaps.goal1:fill(w)
  assets.bitmaps.goal1:fill(b, 1, 2, 1, 1)
  assets.bitmaps.goal1:fill(b, 2, 1, 1, 1)
  assets.bitmaps.goal1:fill(b, 3, 2, 1, 1)
  assets.bitmaps.goal1:fill(b, 2, 3, 1, 1)

  assets.bitmaps.goal2:fill(b)
  assets.bitmaps.goal2:fill(w, 1, 2, 1, 1)
  assets.bitmaps.goal2:fill(w, 2, 1, 1, 1)
  assets.bitmaps.goal2:fill(w, 3, 2, 1, 1)
  assets.bitmaps.goal2:fill(w, 2, 3, 1, 1)

  assets.bitmaps.win:fill(w)
  assets.bitmaps.win:fill(b, 2, 1, 1, 1)
  assets.bitmaps.win:fill(b, 5, 1, 1, 1)
  assets.bitmaps.win:fill(b, 1, 3, 1, 1)
  assets.bitmaps.win:fill(b, 6, 3, 1, 1)
  assets.bitmaps.win:fill(b, 2, 4, 4, 1)

  assets.bitmaps.lose:fill(w)
  assets.bitmaps.lose:fill(b, 2, 1, 1, 1)
  assets.bitmaps.lose:fill(b, 5, 1, 1, 1)
  assets.bitmaps.lose:fill(b, 1, 4, 1, 1)
  assets.bitmaps.lose:fill(b, 6, 4, 1, 1)
  assets.bitmaps.lose:fill(b, 2, 3, 4, 1)

  container.assets = assets
end

container.createGame = function(opt)
  opt = opt or {}
  -- create the game
  local game = Game:new({
    background = Colour:white(),
    name = "tmaze_visual",
    inputs = {"up", "down", "left", "right", "quit"},
    render = {
      width = 8,
      height = 16,
      channels = 1
    }
  })

  -- store the assets in the game
  game.assets = container.assets

  -- set up some game variables that we will be using
  game.stepScore = 0
  game.goalScore = opt.tmaze_reward_high
  game.badGoalScore = opt.tmaze_reward_low

  game.numSteps = opt.tmaze_length or 6
  game.playerMove = 3
  game.playerStart = Vector2:new(4, 8)

  game.allGoals = { "goal1", "goal2" }

  -- add a game object to kick off game with an event
  local startGameObj = GameObject:new()
  function startGameObj:init(game)
    game.score = 0
    game.events:emit("enterGame")
  end
  game:add(startGameObj)

  -- create event listeners
  game.events:add("enterGame", function(event, data)
    -- clear game
    game.objects = {}

    local corridor_length = 4 + (game.numSteps-1) * game.playerMove - 1

    -- create the wall drawing object
    local wallObj = GameObject:new()
    wallObj.start = Vector2:new(game.playerStart.x - 2, game.playerStart.y - 2)
    function wallObj:draw(game)
      local pix = game.render.canvas
      local x = self.start.x
      local y = self.start.y
      if game.render.cameraEnabled then
    		x = x - (game.render.camera.x - 1)
    		y = y - (game.render.camera.y - 1)
    	end
      local b = Colour:black()
      -- vertical wall at start
      pix:fill(b, x, y, 1, 6)
      -- corridor walls
      pix:fill(b, x, y, corridor_length, 1)
      pix:fill(b, x, y + 5, corridor_length, 1)
      -- end of maze
      local end_start = Vector2:new(x + corridor_length - 1, y)
      pix:fill(b, end_start.x, end_start.y - 4, 1, 4)
      pix:fill(b, end_start.x, end_start.y - 4, 7, 1)
      pix:fill(b, end_start.x, end_start.y + 5, 1, 5)
      pix:fill(b, end_start.x, end_start.y + 9, 7, 1)
      -- back wall
      pix:fill(b, end_start.x + 6, end_start.y - 4, 1, 14)
    end
    game:add(wallObj)


    -- create the bitmap game obj proto
    local bitmapObj = GameObject:new()
    function bitmapObj:draw(game)
      if self.bitmap then
        game.render:draw(self.bitmap, self.position.x, self.position.y)
      end
    end

    -- choose the goal and place the indicator as an object
    game.goal = game.allGoals[math.random(#game.allGoals)]
    local goalObj = Utils.clone(bitmapObj)
    goalObj.position = Vector2:new(3, 2)
    goalObj.bitmap = game.assets.bitmaps[game.goal]
    game:add(goalObj)

    -- place the goal markers
    local goal_on_top = (math.random() < 0.5)
    local other_bitmap = "goal1"
    if game.goal == "goal1" then other_bitmap = "goal2" end

    local goalObj = Utils.clone(bitmapObj)
    goalObj.position = Vector2:new(game.playerStart.x + corridor_length - 1, goal_on_top and 4 or 11)
    goalObj.collider = Collider:new({position = goalObj.position, width = 3, height = 3})
    goalObj.bitmap = game.assets.bitmaps[game.goal]
    goalObj.id = game.goal
    game:add(goalObj)

    local otherObj = Utils.clone(bitmapObj)
    otherObj.position = Vector2:new(game.playerStart.x + corridor_length - 1, goal_on_top and 11 or 4)
    otherObj.collider = Collider:new({position = otherObj.position, width = 3, height = 3})
    otherObj.bitmap = game.assets.bitmaps[other_bitmap]
    otherObj.id = other_bitmap
    game:add(otherObj)

    -- add player
    local player = GameObject:new({
      id = "player",
      position = Vector2:new(game.playerStart.x , game.playerStart.y),
      collider = Collider:new({ position = Vector2:new(game.playerStart.x, game.playerStart.y), width = 2, height = 2})
    })
    player.move = game.playerMove
    player.num_steps = 0
    player.max_steps = game.numSteps

    function player:update(game)
        local x = self.position.x
        local y = self.position.y
        if game.input:isOn("up") then
          self.position.y = self.position.y - self.move
        elseif game.input:isOn("down") then
          self.position.y = self.position.y + self.move
        elseif game.input:isOn("left") then
          self.position.x = self.position.x - self.move
          self.num_steps = self.num_steps - 1
        elseif game.input:isOn("right") then
          self.position.x = self.position.x + self.move
          self.num_steps = self.num_steps + 1
        end

        -- check bounds horizontal
        if self.num_steps < 0 then
          self.position.x = game.playerStart.x
          self.num_steps = 0
        end
        if self.num_steps > self.max_steps then
          self.position.x = game.playerStart.x + (self.max_steps * self.move)
          self.num_steps = self.max_steps
        end

        -- lock vertical movement except at the very end of the corridor
        if self.position.y ~= game.playerStart.y and self.num_steps ~= self.max_steps then self.position.y = game.playerStart.y end

        -- match collider position with player
        self.collider.position:set(self.position)
        -- match the camera x position
        game.render.camera.x = self.position.x - 3
        -- if player moved, decrease score slightly
        if x ~= self.position.x or y ~= self.position.y then
          game.score = game.score + game.stepScore
          if game.score < 0 then game.score = 0 end
        end
    end

    function player:draw(game)
        game.render:draw(game.assets.bitmaps.player, self.position.x, self.position.y)
    end

    function player:collide(game, other)
      if other.id ~= nil then
        local img = game.assets.bitmaps.lose
        if other.id == game.goal then
          img = game.assets.bitmaps.win
          game.score = game.score + game.goalScore
        else
          game.score = game.score + game.badGoalScore
        end
        game.events:emit("gameOver", img)
      end
    end

    game:add(player)

  end)

  game.events:add("gameOver", function(event, data) -- data is the screen text
    game.objects = {}
    game.render.camera.x = 1

    -- add drawing object
    local img = data
    local gameOverObj = GameObject:new({
      position = Vector2:new(game.render.canvas.width/2, game.render.canvas.height/2)
    })
    function gameOverObj:draw(game)
      game.render:draw(img, self.position.x - img.width / 2, self.position.y - img.height / 2)
    end
    function gameOverObj:update(game)
      game.running = false -- game over
    end
    game:add(gameOverObj)
    game.running = false
  end)

  return game
end

return container
