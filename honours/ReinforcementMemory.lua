
local Mem = {}

function Mem:new(opt)
  local mem = {}
  -- calculate the sizes to use for creating the memory storage
  local screen_sizes = opt.screen_sizes
  local state_sizes = torch.LongStorage(1 + #screen_sizes)
  for i = 1, #screen_sizes do
    state_sizes[i + 1] = screen_sizes[i]
  end
  -- create the memory
  mem = { size = opt.memory_size }
  state_sizes[1] = mem.size
  mem.data = torch.FloatTensor(state_sizes)
  mem.data:fill(0)
  -- set metatable
  setmetatable(mem, self)
  self.__index = self
  return mem
end

function Mem:act(action, state)
  -- do memory action
  for i = 1, self.size do
    if action == "mem_write_" .. i then
      self.data[i]:copy(state)
    elseif action == "mem_delete_" .. i then
      self.data[i]:zero()
    end
  end
end

function Mem:actions()
  local actions = {}
  for i = 1, self.size do
    table.insert(actions, "mem_write_" .. i)
    table.insert(actions, "mem_delete_" .. i)
  end
  return actions
end

function Mem:reset()
  self.data:zero()
end

return Mem
