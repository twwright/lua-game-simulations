local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'
local Colour = require 'Colour'
local Vector2 = require 'Vector2'
local Collider = require 'Collider'

local container = {}

container.loadAssets = function(opt)
  -- store all the game assets as a table
  local assets = {}

  -- manually create the assets
  assets.bitmaps = {
    player  =   Bitmap:new({width = 3, height = 3}),
    enemy   =   Bitmap:new({width = 3, height = 3}),
    wall    =   Bitmap:new({width = 3, height = 3}),
    goal    =   Bitmap:new({width = 3, height = 3})
  }

  local b = Colour:black()

  assets.bitmaps.wall:fill(b)

  assets.bitmaps.player:fill(b, 2, 1, 1, 3) -- vertical stripe of player cross
  assets.bitmaps.player:fill(b, 1, 2, 3, 1) -- horizontal stripe of player cross

  assets.bitmaps.goal:fill(Colour:white())
  assets.bitmaps.goal:fill(b, 1, 2, 1, 1)
  assets.bitmaps.goal:fill(b, 2, 1, 1, 1)
  assets.bitmaps.goal:fill(b, 3, 2, 1, 1)
  assets.bitmaps.goal:fill(b, 2, 3, 1, 1)

  assets.bitmaps.enemy:fill(b)
  assets.bitmaps.enemy:fill(Colour:white(), 1, 2, 1, 1)
  assets.bitmaps.enemy:fill(Colour:white(), 2, 1, 1, 1)
  assets.bitmaps.enemy:fill(Colour:white(), 3, 2, 1, 1)
  assets.bitmaps.enemy:fill(Colour:white(), 2, 3, 1, 1)

  container.assets = assets
end

container.createGame = function()
  -- create the game
  local game = Game:new({
    background = Colour:white(),
    name = "test",
    inputs = {"none", "up", "down", "left", "right", "quit"},
    render = {
      width = 16,
      height = 16
    }
  })

  game.assets = container.assets

  -- define some parameters
  game.starts = {
    Vector2:new(1, 1),
    Vector2:new(14, 1),
    Vector2:new(1, 14),
    Vector2:new(14, 14)
  }
  game.enemyStart = Vector2:new(14, 1) -- top-right corner
  game.playerStart = Vector2:new(1, 1) -- top-left corner
  game.goalPos = Vector2:new(14, 14) -- bottom-right corner
  game.wallPositions = {
    Vector2:new(6, 6),
    Vector2:new(9, 9)
  }
  game.stepSize = 1
  game.enemyProb = 0.3

  local gameStarter = GameObject:new()
  game:add(gameStarter)
  function gameStarter:init()
    -- reset the score
    game.score = 0

    -- reset the game objects
    game:removeAll(game.objects)
    game:add(gameStarter) -- re-add the game starter

    -- create the enemy object
    local enemy = GameObject:new({
      id = "enemy",
      position = Vector2:new(game.enemyStart.x , game.enemyStart.y),
      collider = Collider:new({ position = Vector2:new(game.enemyStart.x, game.enemyStart.y), width = 3, height = 3, solid = true})
    })

    function enemy:draw(game)
      game.render:draw(game.assets.bitmaps.enemy, self.position.x, self.position.y)
    end

    -- this function called when the player moves
    function enemy:move(game, player)
      local moves = {
        Vector2:new(0, 0), -- none
        Vector2:new(game.stepSize, 0), -- right
        Vector2:new(-game.stepSize, 0), -- left
        Vector2:new(0, game.stepSize), -- down
        Vector2:new(0, -game.stepSize), -- up
      }
      local move = moves[torch.random(#moves)]
      if math.random() < game.enemyProb then
        local diff = player.position:copy():sub(self.position.x, self.position.y)
        move.x = Utils.clamp(math.floor(diff.x), -1, 1)
        move.y = Utils.clamp(math.floor(diff.y), -1, 1)
      end
      self.position:add(move.x, move.y)

      -- check bounds
      self.position.x = Utils.clamp(self.position.x, 1, game.render.canvas.width - 2)
      self.position.y = Utils.clamp(self.position.y, 1, game.render.canvas.height - 2)

      -- match collider position with enemy
      self.collider.position:set(self.position)
    end

    game:add(enemy)

    -- create goal object
    local goal = GameObject:new({
      id = 'goal',
      position = Vector2:new(game.goalPos.x , game.goalPos.y),
      collider = Collider:new({ position = Vector2:new(game.goalPos.x, game.goalPos.y), width = 3, height = 3})
    })

    function goal:draw(game)
      game.render:draw(game.assets.bitmaps.goal, self.position.x, self.position.y)
    end

    game:add(goal)

    -- create wall prototype
    local wall = GameObject:new({
      id = 'wall',
      position = Vector2:new(1, 1),
      collider = Collider:new({ position = Vector2:new(1, 1), width = 3, height = 3, solid = true, static = true})
    })

    function wall:draw(game)
      game.render:draw(game.assets.bitmaps.wall, self.position.x, self.position.y)
    end

    -- spawn walls
    for i = 1, #game.wallPositions do
      local wall_clone = Utils.clone(wall)
      wall_clone.position = Vector2:new():set(game.wallPositions[i])
      wall_clone.collider = Utils.clone(wall.collider)
      wall_clone.collider.position = wall_clone.position:copy()
      game:add(wall_clone)
    end

    -- create player object
    local player = GameObject:new({
      id = "player",
      position = Vector2:new(game.playerStart.x , game.playerStart.y),
      collider = Collider:new({ position = Vector2:new(game.playerStart.x, game.playerStart.y), width = 3, height = 3, solid = true})
    })

    function player:update(game)
        local x = self.position.x
        local y = self.position.y
        if game.input:isOn("up") then
          self.position.y = self.position.y - game.stepSize
        end
        if game.input:isOn("down") then
          self.position.y = self.position.y + game.stepSize
        end
        if game.input:isOn("left") then
          self.position.x = self.position.x - game.stepSize
        end
        if game.input:isOn("right") then
          self.position.x = self.position.x + game.stepSize
        end

        if x ~= self.position.x or y ~= self.position.y then
          enemy:move(game, self)
        end

        -- check bounds
        self.position.x = Utils.clamp(self.position.x, 1, game.render.canvas.width - 2)
        self.position.y = Utils.clamp(self.position.y, 1, game.render.canvas.height - 2)

        -- match collider position with player
        self.collider.position:set(self.position)
    end

    function player:draw(game)
        game.render:draw(game.assets.bitmaps.player, self.position.x, self.position.y)
    end

    function player:collide(game, other)
      if other.id == 'enemy' then
        -- game over!
        self.collider.solid = false -- stop the collision from resolving
        game.score = game.score - 1
        game:remove(player)
        game.running = false
      elseif other.id == 'goal' then
        -- win!
        game.score = game.score + 1
        game:remove(other)
        game.running = false
      end
    end

    game:add(player)
  end -- end game:init()

  return game
end

return container
