local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'
local Colour = require 'Colour'
local Vector2 = require 'Vector2'
local Collider = require 'Collider'

local container = {}

container.loadAssets = function(opt)
  container.assets = {} -- T-Maze doesn't use any assets
end

container.createGame = function(opt)
  opt = opt or {}
  -- create the game
  local game = Game:new({
    background = Colour:white(),
    name = "tmaze_visual",
    inputs = {"up", "down", "left", "right", "quit"},
    render = {
      width = 6,
      height = 1,
      channels = 1,
    }
  })

  -- set up game parameters
  game.length = opt.tmaze_length or 8
  game.reward_high = opt.tmaze_reward_high or 4
  game.reward_low = opt.tmaze_reward_low or -0.1

  -- add the game object that runs the game
  local obj = GameObject:new()
  function obj:init(game)
    game.objects = { self }

    --game._state = torch.ByteTensor(6):zero()

    game.is_high_reward_up = math.random() < 0.5
    game.x_pos = 1
    game.y_pos = 0
    game.score = 0
  end

  function obj:update(game)
    -- perform horizontal movement
    local old_x = game.x_pos
    if game.input:isOn("left") then
      game.x_pos = game.x_pos - 1
    elseif game.input:isOn("right") then
      game.x_pos = game.x_pos + 1
    end
    game.x_pos = Utils.clamp(game.x_pos, 1, game.length)

    -- vertical movement if at the final spot
    if game.x_pos == game.length then
      if game.input:isOn("up") then
        game.y_pos = 1
      elseif game.input:isOn("down") then
        game.y_pos = -1
      end
    end

    if game.y_pos == 0 and old_x ~= game.x_pos then
      --game.score = game.score + game.reward_low
    end
  end

  function obj:draw(game)
    -- update the state, treating it is a 1-d tensor
    local state = game.render.canvas.pixels:squeeze()

    state:zero()

    if game.y_pos == 0 then
      game.running = true
      state[5] = 1 -- can move left initially
      state[6] = 1 -- can move right initially
      if game.x_pos == 1 then -- at the start
        state[5] = 0 -- disable moving left
        if game.is_high_reward_up then
          state[1] = 1 -- indicate that at the T, high reward is up
        else
          state[2] = 1 -- indicate that at the T, high reward is down
        end
      elseif game.x_pos == game.length then
        state[6] = 0 -- disable moving right
        state[3] = 1 -- can move up
        state[4] = 1 -- can move down
      end
    elseif game.y_pos == 1 then
      state[4] = 1 -- can move down
      if game.is_high_reward_up then game.score = game.score + game.reward_high else game.score = game.score + game.reward_low end
      game.running = false
    elseif game.y_pos == -1 then
      state[3] = 1 -- can move up
      if not game.is_high_reward_up then game.score = game.score + game.reward_high else game.score = game.score + game.reward_low end
      game.running = false
    end
  end

  game:add(obj)

  return game
end

return container
