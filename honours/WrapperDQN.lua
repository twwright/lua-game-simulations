local Utils = require 'Utils'

local env = {}

-- constructor
function env:init(opt)
  self.opt = Utils.clone(opt)
  -- call reset to create the game
  self:reset(opt)
  self._reward = 0
  self._term = false
  self.actions = self.game.input:getInputsList()
  for _, bad_action in pairs(opt.remove_actions or {}) do
    Utils.remove(self.actions, bad_action)
  end
  return self
end

-- retrieve the current state of the game: screen, current reward, if the game is over
function env:getState()
  local state = self.game._state or self.game.render.canvas.pixels
  if not self._state or self._state:nElement() ~= state:nElement() then
    self._state = state:clone()
  else
    self._state:copy(state)
  end
  return self._state, self._reward, self._term
end

-- reset the game state to the beginning of the game
function env:reset(opt)
  -- load the assets
  self.opt.game_class.loadAssets(opt)
  -- create the game
  self.game = self.opt.game_class.createGame(self.opt)
  if self.opt.reset_func then
    self.opt.reset_func(self.game, self.opt)
  end
  self.game.name = self.opt.game_name or self.game.name
  -- clear temporary variables
  self._reward = 0
  self._term = false
  -- add in listeners
  self.game.listeners = self.opt.listeners or {}
  -- init the game, step once to refresh screen
  self.game:init()
  self.game:step()
end

-- advance the game one step, returning the screen state, reward at this step, if the game is still running, and the number of lives
function env:step(action, training)
  -- remove all previous actions (so DQN doesn't trip up on pressing select)
  for k, _ in pairs(self.game.input.prevInputs) do
    self.game.input.prevInputs[k] = false
  end
  -- update the action in the game
  self.game.input.inputs[action] = true

  -- cache the number of lives
  local lives = self.game.lives
  local score = self.game.score
  -- advance the game
  self.game:step()
  -- check if we are training and the lives just dropped
  self._reward = self.game.score - score
  if training and lives ~= nil and self.game.lives < lives then
    self._term = true
  else
    self._term = not self.game.running
  end

  return self:getState()
end

-- starts a new game and returns the state of it
function env:newGame()
  self:reset(self.opt)
  return self:getState()
end

-- restarts the game and then takes k no-op actions and returns the state of it
function env:nextRandomGame(k)
  self:newGame()
  k = k or torch.random(5)
  for i = 1, k-1 do
    self:step('none')
  end
  return self:getState()
end

function env:nObsFeature()
  return self.game.render.canvas.pixels:nElement()
end

function env:getActions()
  return self.actions
end

return env
