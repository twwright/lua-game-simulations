-- manages reinforcement memory
-- - three new actions are added to the game: mem_write, mem_inc, mem_reset
-- - at each time step, the current recalled memory is concatenated to the current screen
-- - combination state stored in game._state

-- this listener must be added AFTER any listeners that set inputs

local GameListener = require 'GameListener'
local Input = require 'Input'
local Utils = require 'Utils'
local ReinforcementMemory = require 'ReinforcementMemory'

local ReinforcementMemoryListener = GameListener:new()

function ReinforcementMemoryListener:new(opts)
  opts = opts or {}
  assert(opts.memory_size, 'Memory size required')
  local listener = {
    memory = ReinforcementMemory:new({
      memory_size = opts.memory_size,
      screen_sizes = opts.screen_sizes,
    })
  }
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function ReinforcementMemoryListener:init(game)
  -- if memory actions not already added to game, add them
  local inputs = game.input.inputs
  local mem_actions = self.memory:actions()
  local mem_actions_added = true
  for _, a in pairs(mem_actions) do
    if not Utils.contains(inputs, a) then mem_actions_added = false end
  end
  -- add memory_actions if required
  if not mem_actions_added then
    local game_actions = game.input:getInputsList()
    for _, mem_a in ipairs(mem_actions) do
      game_actions[#game_actions + 1] = mem_a
    end
    game.input = Input:new({inputs = game_actions})
  end
  -- create game._state for use
  local screen_sizes = game.render.canvas.pixels:size()
  local state_sizes = torch.LongStorage(1 + #screen_sizes)
  for i = 1, #screen_sizes do
    state_sizes[i + 1] = screen_sizes[i]
  end
  state_sizes[1] = self.memory.size + 1
  game._state = torch.zeros(state_sizes):float()
end

function ReinforcementMemoryListener:preUpdate(game)
  -- perform a memory action if any are active
  local mem_actions = self.memory:actions()
  for action, is_active in pairs(game.input.inputs) do
    if is_active and Utils.contains(mem_actions, action) then
      self.memory:act(action, game.render.canvas.pixels)
    end
  end
end

function ReinforcementMemoryListener:postUpdate(game)
  -- update game._state so reinforcement memory state can be utilised
  game._state[1]:copy(game.render.canvas.pixels)
  game._state[{{2, game._state:size(1)}}]:copy(self.memory.data)
end

return ReinforcementMemoryListener
