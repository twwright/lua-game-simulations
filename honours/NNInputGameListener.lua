require 'nn'

local GameListener = require 'GameListener'

local NNInputGameListener = GameListener:new()

function NNInputGameListener:new(opt)
  opt = opt or {}
  assert(opt.network, 'Network not provided!')
  assert(opt.actions, 'Action names must be provided!')
  local listener = {
    network = opt.network,
    preproc_net = opt.preproc_net,
    preproc_func = opt.preproc_func,
    actions = opt.actions,
    rand_prob = opt.rand_prob or 0.1
  }
  if opt.softmax then
    listener.softmax = nn.SoftMax():float()
    listener.temperature = opt.temperature or 1
  end
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function NNInputGameListener:init()
  -- do nothing
end

function NNInputGameListener:preUpdate(game)
  -- feed the game state through the network
  local action = self.actions[torch.random(1, #self.actions)]
  if math.random() > self.rand_prob then -- if NOT random choice
    local state = game._state or game.render.canvas.pixels
    -- apply preprocessing
    if self.preproc_net then
      state = self.preproc_net:forward(state)
    end
    if self.preproc_func then
      state = self.preproc_func(state)
    end
    -- forward pass through agent network
    state = state:clone():float():reshape(state:nElement())
    local q = self.network:forward(state)
    self._q = q
    -- select action
    if self.softmax then
      -- softmax the q-values and sample an action
      q:div(self.temperature)
      local sm = self.softmax:forward(q)
      self._sm = sm
      action = self.actions[torch.multinomial(sm, 1)[1]] -- sample once from the softmax distribution
    else
      -- select the max value actions
      local maxq = q[1]
      local besta = {1}
      for a = 2, #self.actions do
          if q[a] > maxq then
              besta = { a }
              maxq = q[a]
          elseif q[a] == maxq then
              besta[#besta+1] = a
          end
      end
      action = self.actions[besta[torch.random(1, #besta)]]
    end
  end
  self._action = action

  -- clear previous actions so any action counts as just pressed
  for i, v in ipairs(self.actions) do
    game.input.prevInputs[action] = false
  end

  -- set the action in the game
  game.input.inputs[action] = true
end

return NNInputGameListener
