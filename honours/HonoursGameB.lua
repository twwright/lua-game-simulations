local Game = require 'Game'
local Bitmap = require 'Bitmap'
local Font = require 'Font'
local Utils = require 'Utils'
local GameObject = require 'GameObject'

local container = {}

container.loadAssets = function()
  -- store all the game assets as a table
  local assets = {}
  -- load the sprite bitmaps
  local gameAtlas = Bitmap:load('atlas2.png')
  assets.bitmaps = {
    wall    =   gameAtlas:crop(1, 1, 8, 8),
    player  =   gameAtlas:crop(9, 1, 8, 8),
    door    =   gameAtlas:crop(17, 1, 8, 8),
    door2   =   gameAtlas:crop(25, 1, 8, 8),
    sage    =   gameAtlas:crop(33, 1, 8, 8),
    blackKnight    =   gameAtlas:crop(41, 1, 8, 8),
    key     =   gameAtlas:crop(49, 1, 8, 8),
    death   =   gameAtlas:crop(57, 1, 8, 8),
    edge    =   gameAtlas:crop(1, 9, 8, 8),
    corner  =   gameAtlas:crop(9, 9, 8, 8),
    grass   =   gameAtlas:crop(17, 9, 8, 8),
    grass2  =   gameAtlas:crop(25, 9, 8, 8),
    chest   =   gameAtlas:crop(33, 9, 8, 8),
    whiteKnight    =   gameAtlas:crop(41, 9, 8, 8),
    armour  =   gameAtlas:crop(49, 9, 8, 8),
    blank   =   gameAtlas:crop(57, 9, 8, 8)
  }

  -- manipulate edge and corner to create all variations
  assets.bitmaps.edges = {
    ht = assets.bitmaps.edge:manip('V'),
    hb = assets.bitmaps.edge,
    vl = assets.bitmaps.edge:manip('R'),
    vr = assets.bitmaps.edge:manip('L')
  }

  assets.bitmaps.corners = {
    tr = assets.bitmaps.corner,
    tl = assets.bitmaps.corner:manip('L'),
    br = assets.bitmaps.corner:manip('R'),
    bl = assets.bitmaps.corner:manip('VH')
  }

  -- load the font
  assets.font = Font:load('pixel.font')

  container.assets = assets
end

container.createGame = function()
  -- create the game
  local game = Game:new({
    name = "honours",
    inputs = {"up", "down", "left", "right", "select"},
    render = {
      width = 96,
      height = 54
    }
  }) -- TODO: parameters for creating the game

  -- store the assets in the game
  game.assets = container.assets

  -- set up some game variables that we will be using
  game.goal = 1
  game.numGoals = 5
  game.allGoals = {
    {
      name = "sage",
      bitmap = game.assets.bitmaps.sage
    },
    {
      name = "key",
      bitmap = game.assets.bitmaps.key
    },
    {
      name = "chest",
      bitmap = game.assets.bitmaps.chest
    },
    {
      name = "door",
      bitmap = game.assets.bitmaps.door
    },
    {
      name = "door2",
      bitmap = game.assets.bitmaps.door2
    },
    {
      name = "blackKnight",
      bitmap = game.assets.bitmaps.blackKnight
    },
    {
      name = "whiteKnight",
      bitmap = game.assets.bitmaps.whiteKnight
    },
    {
      name = "death",
      bitmap = game.assets.bitmaps.death
    },
    {
      name = "armour",
      bitmap = game.assets.bitmaps.armour
    }
  }
  game.goals = {
    {
      name = "sage",
      bitmap = game.assets.bitmaps.sage
    },
    {
      name = "key",
      bitmap = game.assets.bitmaps.key
    },
    {
      name = "chest",
      bitmap = game.assets.bitmaps.chest
    }
  }

  -- create the goal generator
  local goalGenerator = GameObject:new()
  function goalGenerator:init(game)
    local goals = {}
    local goalStr = ""
    local rand
    for i = 1, game.numGoals do
      repeat
        rand = math.random(#game.allGoals)
        goalStr = goalStr .. rand
        goals[i] = game.allGoals[rand]
      until i == 1 or goals[i] ~= goals[i-1]
    end
    print('Goals: ' .. goalStr)
    game.goals = goals
  end

  game:add(goalGenerator)

  -- create the goal text objects
  local goalW, goalH = game.assets.font:size("GOAL " .. game.goal)
  local goalText = GameObject:new({
    position = Vector2:new(game.render.canvas.width/2 - goalW/2, game.render.canvas.height - goalH)
  })
  function goalText:draw(game)
    game.render:text(game.assets.font, "GOAL " .. game.goal, self.position.x, self.position.y)
  end

  game:add(goalText)

  -- create an object to receive user input and advance screen
  local inputObj = GameObject:new()
  function inputObj:update(game)
    if game.input:justOn('select') then
      if game.goal < #game.goals then
        game.goal = game.goal + 1
      else
        game.running = false
      end
    end
  end

  game:add(inputObj)

  local goalView = GameObject:new({
    position = Vector2:new(game.render.canvas.width/2 - 4, game.render.canvas.height/2 - 4)
  })
  function goalView:draw(game)
    -- render goal
    local x = self.position.x
    local y = self.position.y
    game.render:draw(game.goals[game.goal].bitmap, self.position.x, self.position.y)
    -- render border
    game.render:draw(game.assets.bitmaps.corners.tl, x - 8, y - 8) -- top-left
    game.render:draw(game.assets.bitmaps.corners.br, x + 8, y + 8) -- bottom-right
    game.render:draw(game.assets.bitmaps.corners.bl, x - 8, y + 8) -- bottom-left
    game.render:draw(game.assets.bitmaps.corners.tr, x + 8, y - 8) -- top-right
    game.render:draw(game.assets.bitmaps.edges.vr, x - 8, y) -- left
    game.render:draw(game.assets.bitmaps.edges.vl, x + 8, y) -- right
    game.render:draw(game.assets.bitmaps.edges.ht, x, y + 8) -- bottom
    game.render:draw(game.assets.bitmaps.edges.hb, x, y - 8) -- top
  end

  game:add(goalView)

  -- add goals
  local goal = GameObject:new({
    position = Vector2:new(1, 1),
    collider = Collider:new({position = Vector2:new(1, 1), width = 8, height = 8})
  })

  function goal:update(game)
    -- nothing
  end

  function goal:draw(game)
    game.render:draw(self.bitmap, self.position.x, self.position.y)
  end

  function goal:collide(game, other)
    -- Nothing!
  end

  --[[
  local goal1 = Utils.clone(goal)
  goal1.bitmap = game.assets.bitmaps.sage
  goal1.position = Vector2:new(50, 8)
  goal1.collider = Collider:new({position = Vector2:new(50, 8), width = 8, height = 8})
  goal1.id = "goal1"
  --]]
  return game
end

return container
