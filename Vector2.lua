-- Vector2 class for LGS

local Vector2 = {}

function Vector2:new(x, y)
	local vec2 = {x = x, y = y}
	setmetatable(vec2, self)
	self.__index = self
	return vec2
end

function Vector2:add(x, y)
	self.x = self.x + x
	self.y = self.y + y
	return self
end

function Vector2:sub(x, y)
	self.x = self.x - x
	self.y = self.y - y
	return self
end

function Vector2:scl(n)
	self.x = self.x * n
	self.y = self.y * n
	return self
end

function Vector2:set(vec2Orx, y)
	if type(vec2Orx) == 'table' then
		self.x = vec2Orx.x
		self.y = vec2Orx.y
	else
		self.x = vec2Orx
		self.y = y
	end
	return self
end

function Vector2:copy()
	return Vector2:new(self.x, self.y)
end

function Vector2.zero()
	return Vector2:new(0, 0)
end

function Vector2.one()
	return Vector2:new(1, 1)
end

return Vector2
