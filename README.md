# Lua Game Simulations #

A game framework written in Lua/Torch for creating basic 2D games. Created to assist with a research project into deep reinforcement learning.