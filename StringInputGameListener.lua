local GameListener = require 'GameListener'

local StringInputGameListener = GameListener:new()

function StringInputGameListener:new(opts)
  opts = opts or {}
  assert(opts.inputMap, 'Input-character map must be defined!')
  assert(opts.input, 'Input string must be provided!')
  local listener = {
    inputMap = opts.inputMap,
    input = opts.input,
    current = 1
  }
  setmetatable(listener, self)
  self.__index = self
  return listener
end

function StringInputGameListener:init()
  -- reset input pointer to the start
  self.current = 1
end

function StringInputGameListener:preUpdate(game)
  -- get the current input from the string,
  -- match it with the correct input, and set it to true in the game
  local char = self.input:sub(self.current, self.current)
  print(string.format('StringInputGameListener: \'%s\' => %s', char, self.inputMap[char]))
  if char ~= ' ' then
    game.input.inputs[self.inputMap[char]] = true
  end
  self.current = self.current + 1
  if self.current > self.input:len() then
    self.current = 1 -- reset current if out of inputs
  end
end

return StringInputGameListener
