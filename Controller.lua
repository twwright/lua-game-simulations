-- Controller class for LGS: Manages the game loop of a Game, handling updating of GameListeners

local Controller = {}

function Controller:new(opts)
  opts = opts or {}
  local controller = {
    game = opts.game,
    listeners = opts.listeners or {}
  }
  setmetatable(controller, self)
  self.__index = self
  return controller
end

function Controller:run()
  -- initialise the game
  self.game:init()
  -- initialise the listeners
  for _, listener in pairs(self.listeners) do
    listener:init(self.game)
  end
  -- step game once to render first screen
  self.game:step()
  -- run game loop
  while self.game.running do
    -- notify listeners
    for _, listener in pairs(self.listeners) do
      listener:preUpdate(self.game)
    end
    -- update game
    self.game:step()
    -- notify listeners
    for _, listener in pairs(self.listeners) do
      listener:postUpdate(self.game)
    end
  end
  -- notify listeners of finish
  for _, listener in pairs(self.listeners) do
    listener:finish(self.game)
  end
end

return Controller
