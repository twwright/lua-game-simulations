-- Input class for LGS

local Input = {}

function Input:new(opts)
	opts = opts or {}
	local input = {inputsList = opts.inputs, inputs = {}, prevInputs = {}}
	-- initialise all actions as off
	for _, v in pairs(opts.inputs or {}) do
		input.inputs[v] = false
		input.prevInputs[v] = false
	end
	setmetatable(input, self)
	self.__index = self
	return input
end

function Input:update()
	-- update the cache
	for k, _ in pairs(self.inputs or {}) do
		self.prevInputs[k] = self.inputs[k]
		self.inputs[k] = false
	end
end

function Input:justOn(input)
	return (not self.prevInputs[input]) and self.inputs[input]
end

function Input:isOn(input)
	return self.inputs[input]
end

function Input:getInputsList()
	return self.inputsList
end

return Input
