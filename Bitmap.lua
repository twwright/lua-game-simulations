-- Requires torch and torch image
require 'torch'
require 'image'
local Utils = require 'Utils'

local Bitmap = {}

function Bitmap:new(opts)
  local bitmap = {
    width = opts.width,
    height = opts.height,
    channels = opts.channels or 3,
    pixels = opts.pixels or torch.FloatTensor(opts.channels or 3, opts.height, opts.width):fill(1.0),
    alpha = opts.alpha or torch.FloatTensor(opts.height, opts.width):fill(1.0)
  }
  setmetatable(bitmap, self)
  self.__index = self
  return bitmap
end

-- Note: this function assumes RGBA PNG files!
function Bitmap:load(file)
  local rawImg = image.load(file)
	local w = rawImg:size(3)
	local h = rawImg:size(2)
	return Bitmap:new({
		width = w,
		height = h,
    channels = 3,
		pixels = torch.FloatTensor(3, h, w):copy(rawImg[{{1, 3}}]), -- first three channels
		alpha = torch.FloatTensor(h, w):copy(rawImg[{{4}}]) -- fourth channel
	})
end

-- Returns the cropped section of the bitmap as a new bitmap. Does not modify the original
function Bitmap:crop(cropX, cropY, cropWidth, cropHeight)
	local minX = Utils.clamp(cropX, 1, self.width)
	local maxX = Utils.clamp(cropX + cropWidth - 1, 1, self.width)
	local minY = Utils.clamp(cropY, 1, self.height)
	local maxY = Utils.clamp(cropY + cropHeight - 1, 1, self.height)
	local w = maxX - minX + 1
	local h = maxY - minY + 1
	local crop = Bitmap:new({
		width = w,
		height = h,
		pixels = torch.FloatTensor(self.channels, h, w):copy(self.pixels[{{}, {minY, maxY}, {minX, maxX}}]),
		alpha = torch.FloatTensor(h, w):copy(self.alpha[{{minY, maxY}, {minX, maxX}}])
	})
	return crop
end

-- Returns a flattened copy of the bitmap (alpha applied)
function Bitmap:flatten(background)
	local output = torch.FloatTensor(self.channels, self.height, self.width)
	local colours = { background.r, background.g, background.b}
	for i = 1, self.channels do -- channels
		output[{{i}}]:map2(self.pixels[{{i}}], self.alpha,
			function(colourOutput, colourBitmap, alphaBitmap)
				return Utils.clamp(colours[i] * (1 - alphaBitmap) + colourBitmap * alphaBitmap, 0.0, 1.0)
			end)
	end
	return Bitmap:new({
    width = self.width,
    height = self.height,
    channels = self.channels,
    pixels = output,
    alpha = torch.FloatTensor(self.height, self.width):fill(1.0)
  })
end

function Bitmap:fill(colour, x, y, w, h)
	colour = colour or Colour:new()
	x = x or 1
	y = y or 1
	w = w or self.width
	if w < 1 then w = 1 end
	h = h or self.height
	if h < 1 then h = 1 end
	local iX, iY, iW, iH = Utils.rectIntersect(1, 1, self.width, self.height, x, y, w, h)
	-- exit early if we have no intersection to colour
	if iW < 1 or iH < 1 then
		return
	end
	-- draw to canvas
	local colours = { colour.r, colour.g, colour.b}
	local wRange = { iX, iX + iW - 1}
	local hRange = { iY, iY + iH - 1}
	for i = 1, self.pixels:size(1) do -- channels
		self.pixels[{i, hRange, wRange}]:fill(colours[i])
	end
  self.alpha[{hRange, wRange}]:fill(colour.a)
end

-- returns a copy of the bitmap manipulated by the sequence of operations specified by operationString
function Bitmap:manip(operationString)
  local operations = {}
  local func
  for i = 1, operationString:len() do
    local char = operationString:sub(i, i)
    if char == 'L' then -- left rotate
      func = function(x, y, w, h) return y, (w - x) + 1, h, w end
    elseif char == 'R' then -- right rotate
      func = function(x, y, w, h) return (h - y) + 1, x, h, w end
    elseif char == 'V' then -- vertical flip
      func = function(x, y, w, h) return x, (h - y) + 1, w, h end
    elseif char == 'H' then -- horizontal flip
      func = function(x, y, w, h) return (w - x) + 1, y, w, h end
    else
      func = function(x, y, w, h) return x, y end
    end
    operations[i] = func
  end
  -- create new pixels
  local w, h = self.pixels:size(3), self.pixels:size(2)
  for op = 1, #operations do
    _, _, w, h = operations[op](1, 1, w, h)
  end
  local newPixels = torch.FloatTensor(self.pixels:size(1), h, w)
  local newAlpha = torch.FloatTensor(h, w)
  -- perform manipulation on each pixel
  local newX, newY, newW, newH
  for i = 1, self.pixels:size(1) do
    for y = 1, self.pixels:size(2) do
      for x = 1, self.pixels:size(3) do
        newX = x
        newY = y
        newW = self.pixels:size(3)
        newH = self.pixels:size(2)
        for op = 1, #operations do
          newX, newY, newW, newH = operations[op](newX, newY, newW, newH)
        end
        newPixels[{i, newY, newX}] = self.pixels[{i, y, x}]
        if i == 1 then
          newAlpha[{newY, newX}] = self.alpha[{y, x}]
        end
      end
    end
  end
  return Bitmap:new({
    width = newAlpha:size(2),
    height = newAlpha:size(1),
    pixels = newPixels,
    alpha = newAlpha
  })
end

return Bitmap
